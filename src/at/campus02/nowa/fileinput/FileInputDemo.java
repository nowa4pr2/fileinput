package at.campus02.nowa.fileinput;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
/**
 * 
 * Diese AppliKation liest Byte aus dem File demo-text.txt.
 * Das gelesene Byte wird in Char konvertiert und anschließend in der Konsole ausgegeben. 
 *
 */
public class FileInputDemo {

	public static void main(String[] args) {
		//Deklaration
		File file = null;
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		
		try {
			file = new File("C:\\devdata\\PR2\\demo-text.txt");
			fis = new FileInputStream(file);
			
			bis = new BufferedInputStream(fis);
			int byteRead = 0;
			
			while ((byteRead = bis.read()) != -1)
			{
				
				char[]b  = Character.toChars(byteRead);
				System.out.print(b[0]);
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally
		{
			
			if (bis != null) {
				try {
					bis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
